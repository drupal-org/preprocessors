# Preprocessors

- [Introduction](#introduction)
- [Purpose](#purpose)
- [Features](#features)
- [How To Use](#how-to-use)

## Introduction
So you have a basic Drupal installation coupled with a nice theme, and
you want to customize the variables the templates receive. Normally,
you'd create a `THEMENAME.theme` file at the root of your theme and add
**Preprocess Hooks** to alter the `$variables` variable. But let's just
say you have a lot of preprocessing to do? This file can become huge,
messy and difficult to manage.

Introducing the new **Preprocessors** module! This module uses the
powerful **Plugin API** to allow for preprocessing management in Plugins
rather than through procedural code in the theme.

## Purpose

The ultimate goal of this module is to better organize preprocessing of
templates in Drupal and to make it fit seamlessly into the new OOP
approach Drupal is taking.

## Features
- Preprocess template variables through the Plugin API.

## How To Use

### Basing yourself on examples

The fastest way to get started with this module is to check and copy
code from the Examples module found within the module. It's so
straightforward that I'm going to stop myself from explaining it here.

Refer to the example plugins to get a gist of it!
