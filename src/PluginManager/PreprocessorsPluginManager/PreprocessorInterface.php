<?php

namespace Drupal\preprocessors\PluginManager\PreprocessorsPluginManager;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Provides an interface for Preprocessor plugins.
 *
 * @package Drupal\preprocessors\PluginManager\PreprocessorsPluginManager
 */
interface PreprocessorInterface extends PluginInspectionInterface {

  /**
   * Return the ID of the preprocessor.
   *
   * @return string
   *   Return the ID of the Preprocessor.
   */
  public function getId(): string;

  /**
   * Return the template of the Preprocessor.
   *
   * @return string
   *   Return the template that the preprocessor is applying alterations to.
   */
  public function getTemplate(): string;

  /**
   * Return the themes this Preprocessor will be applied in.
   *
   * @return array
   *   Return the theme that the preprocessor is applying alterations in.
   */
  public function getThemes(): array;

  /**
   * Return the weight of the Preprocessor.
   *
   * @return int
   *   Return the weight of this preprocessor.
   */
  public function getWeight(): int;

  /**
   * Alter the data provided to the Preprocessor from it's groomer.
   *
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info);

}
