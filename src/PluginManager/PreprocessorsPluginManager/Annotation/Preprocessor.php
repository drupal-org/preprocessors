<?php

namespace Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorPluginBase as PluginBase;

/**
 * Defines a Preprocessor item annotation object.
 *
 * Plugin Namespace: Plugin\Preprocessor.
 *
 * @see plugin_api
 *
 * @package Drupal\preprocessors\Annotation
 *
 * @Annotation
 */
final class Preprocessor extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The target template suggestion to preprocess variables for.
   *
   * @var string
   */
  public $template;

  /**
   * The target theme to preprocess variables in.
   *
   * @var array
   */
  public $themes;

  /**
   * The weight of this preprocessor.
   *
   * @var string
   */
  public $weight = 0;

  /**
   * {@inheritdoc}
   */
  public function getTemplate(): string {
    return $this->definition[PluginBase::TEMPLATE];
  }

  /**
   * {@inheritdoc}
   */
  public function getThemes(): array {
    return $this->definition[PluginBase::THEMES];
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->definition[PluginBase::WEIGHT] ?? 0;
  }

}
