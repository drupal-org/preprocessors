<?php

namespace Drupal\preprocessors\PluginManager\PreprocessorsPluginManager;

use Drupal\Component\Plugin\Discovery\DiscoveryInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscoveryDecorator;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\Annotation\Preprocessor as Annotation;

/**
 * Provides a Plugin Manager for Preprocessors.
 *
 * @package Drupal\preprocessors\PluginManager
 */
final class PreprocessorsPluginManager extends DefaultPluginManager {

  /**
   * Drupal's Theme Handler injected through DI.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Drupal's Theme Manager injected through DI.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Array of preprocessors to work with, stored statically.
   *
   * This variable is static to ensure that it is only stored once.
   *
   * @var array
   */
  public static $preprocessors;

  /**
   * Constructs an PreprocessorsPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler to allow for theme plugin discovery.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager injected through DI.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    ThemeHandlerInterface $theme_handler,
    ThemeManagerInterface $theme_manager
  ) {
    parent::__construct(
      'Plugin/preprocessors',
      $namespaces,
      $module_handler,
      PreprocessorInterface::class,
      Annotation::class
    );

    $this->alterInfo('preprocessors_info');
    $this->setCacheBackend($cache_backend, 'preprocessors');
    $this->themeHandler = $theme_handler;
    $this->themeManager = $theme_manager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery(): DiscoveryInterface {
    if (!$this->discovery) {
      $discovery = new AnnotatedClassDiscovery($this->subdir, $this->namespaces, $this->pluginDefinitionAnnotationName, $this->additionalAnnotationNamespaces);
      $discovery = new YamlDiscoveryDecorator($discovery, 'preprocessors', array_merge($this->moduleHandler->getModuleDirectories(), $this->themeHandler->getThemeDirectories()));
      $discovery = new ContainerDerivativeDiscoveryDecorator($discovery);
      $this->discovery = $discovery;
    }

    return $this->discovery;
  }

  /**
   * Get Preprocessor definitions.
   *
   * {@inheritdoc}
   */
  public function getPreprocessors(): array {
    // Get definitions first.
    $definitions = $this->getDefinitions();

    // Definitions found in themes should only be used in that theme.
    // @TODO - Handle parent/child theme relationships.
    foreach ($definitions as $definition) {
      // If a theme with the provider's name doesn't exist, we can stop here.
      if (!$this->themeHandler->themeExists($definition[PreprocessorPluginBase::PROVIDER])) {
        continue;
      }

      // Otherwise, we check if the current theme is the definition's theme.
      if ($this->themeManager->getActiveTheme()->getName() !== $definition[PreprocessorPluginBase::PROVIDER]) {
        // If it isn't, we remove this definition.
        unset($definitions[$definition[PreprocessorPluginBase::ID]]);
      }
      else {
        // If it is, we set the definition's theme to this provider only.
        $definitions[$definition[PreprocessorPluginBase::ID]][PreprocessorPluginBase::THEMES] = [$definition[PreprocessorPluginBase::PROVIDER]];
      }
    }

    // Format the definitions.
    foreach ($definitions as $definition) {
      // If the definition doesn't have a template set, we shouldn't return it.
      if (empty($definition[PreprocessorPluginBase::TEMPLATE])) {
        continue;
      }

      // Set themes.
      $themes = $definition[PreprocessorPluginBase::THEMES] ?? [PreprocessorPluginBase::ALL_THEMES_VALUE];

      // Set the definition in the array according to all of its themes.
      foreach ($themes as $theme) {
        self::$preprocessors[$theme][$definition[PreprocessorPluginBase::TEMPLATE]][$definition[PreprocessorPluginBase::ID]] = $definition;

        // Sort the array of this theme.
        usort(self::$preprocessors[$theme][$definition[PreprocessorPluginBase::TEMPLATE]], 'self::sortPreprocessors');
      }
    }

    // Return the preprocessors.
    return self::$preprocessors;
  }

  /**
   * Sort preprocessors according to their weight.
   *
   * @param array $a
   *   First preprocessor definition.
   * @param array $b
   *   Second preprocessor definition.
   *
   * @return int
   *   Returns a value less than, equal to or greater than zero depending on if
   *   the first argument is considered to be respectively less than, equal to
   *   or greater than the second.
   */
  protected function sortPreprocessors(array $a, array $b): int {
    return $a[PreprocessorPluginBase::WEIGHT] ?? 0 - $b[PreprocessorPluginBase::WEIGHT] ?? 0;
  }

}
