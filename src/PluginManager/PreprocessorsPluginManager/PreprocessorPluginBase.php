<?php

namespace Drupal\preprocessors\PluginManager\PreprocessorsPluginManager;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for Preprocessor plugins.
 *
 * @package Drupal\preprocessors\Plugin
 */
abstract class PreprocessorPluginBase extends PluginBase implements PreprocessorInterface, ContainerFactoryPluginInterface {

  /**
   * Constant for identifier for preprocessors that apply to all themes.
   */
  public const ALL_THEMES_VALUE = '*';

  /**
   * Constant for the id property of a Preprocessor definition.
   */
  public const ID = 'id';

  /**
   * Constant for the provider property of a Preprocessor definition.
   */
  public const PROVIDER = 'provider';

  /**
   * Constant for the template property of a Preprocessor definition.
   */
  public const TEMPLATE = 'template';

  /**
   * Constant for the themes property of a Preprocessor definition.
   */
  public const THEMES = 'themes';

  /**
   * Constant for the weight property of a Preprocessor definition.
   */
  public const WEIGHT = 'weight';

  /**
   * Dependency injection create method override.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The Dependency Injection container.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->pluginDefinition[self::ID];
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplate(): string {
    return $this->pluginDefinition[self::TEMPLATE];
  }

  /**
   * {@inheritdoc}
   */
  public function getThemes(): array {
    return $this->pluginDefinition[self::THEMES];
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->pluginDefinition[self::WEIGHT] ?? 0;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function preprocess(&$variables, $hook, $info);

}
