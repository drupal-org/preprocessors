<?php

namespace Drupal\preprocessors\Service;

use Drupal\Core\Theme\ThemeManager;
use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorPluginBase;
use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorsPluginManager;

/**
 * Provides an object to manage core functionality of the module.
 *
 * @package Drupal\preprocessors\Service
 */
final class PreprocessorsManager {

  /**
   * Theme Manager injected through DI.
   *
   * @var \Drupal\Core\Theme\ThemeManager
   */
  private $themeManager;

  /**
   * Preprocessor Plugin Manager injected through DI.
   *
   * @var \Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorsPluginManager
   */
  private $preprocessorsPluginManager;

  /**
   * PreprocessorsManager constructor.
   *
   * @param \Drupal\Core\Theme\ThemeManager $themeManager
   *   Theme Manager injected through DI.
   * @param \Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorsPluginManager $preprocessorsPluginManager
   *   Preprocessor Plugin Manager injected through DI.
   */
  public function __construct(ThemeManager $themeManager, PreprocessorsPluginManager $preprocessorsPluginManager) {
    $this->themeManager = $themeManager;
    $this->preprocessorsPluginManager = $preprocessorsPluginManager;
  }

  /**
   * Preprocess variables using Preprocessor Plugins.
   *
   * @param array $variables
   *   Array of default variables obtained from the render instance.
   * @param string $hook
   *   The name of the theme hook being invoked. This is not always accurate.
   * @param array $info
   *   Array of information on the current render instance. This parameter is
   *   not even documented on Drupal's main site, but the module's functionality
   *   relies on it. If this breaks, everything breaks.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function preprocess(array &$variables, string $hook, array $info): void {
    // Get the current theme.
    $theme = $this->themeManager->getActiveTheme()->getName();

    // Get the template name of the current render instance.
    $template = $info['template'] ?? NULL;

    // Do nothing if this info is not set.
    if ($template === NULL) {
      return;
    }

    // Set the preprocessor definitions as a static variable.
    $preprocessors = $this->preprocessorsPluginManager->getPreprocessors();

    // First we check if there are any definitions for this theme.
    // If there aren't, we should stop here.
    if (!isset($preprocessors[PreprocessorPluginBase::ALL_THEMES_VALUE]) && !isset($preprocessors[$theme])) {
      return;
    }

    // Get template parts.
    // Example: ['node', 'node--page'].
    $templateParts = $this->getTemplateParts($template);

    // With each part of the template, get Preprocessors matching the part.
    foreach ($templateParts as $template) {
      // Now we check if there are any definitions for this template.
      if (!isset($preprocessors[PreprocessorPluginBase::ALL_THEMES_VALUE][$template]) && !isset($preprocessors[$theme][$template])) {
        continue;
      }

      // Get the array of plugin definitions matching this template.
      $preprocessors = array_merge(
        $preprocessors[$theme][$template] ?? [],
        $preprocessors[PreprocessorPluginBase::ALL_THEMES_VALUE][$template] ?? []
      );

      // Load all of the plugins found that match, and apply them to data.
      foreach ($preprocessors as $preprocessor) {
        // Create plugin instance.
        $preprocessor = $this->preprocessorsPluginManager->createInstance($preprocessor[PreprocessorPluginBase::ID]);

        // If created successfully, run preprocessing on variables.
        if ($preprocessor !== NULL) {
          $preprocessor->preprocess($variables, $hook, $info);
        }
      }
    }

  }

  /**
   * Get the template parts segmented incrementally.
   *
   * In a template named "node--page", we want to make sure that Preprocessors
   * that target both the "node" template and the "node--page" template run.
   *
   * An array will be returned containing
   * each part of the template incrementally appended to each other.
   *
   * Example: A template like "node--page" will return an array
   * with the following values:
   * ['node', 'node--page'].
   *
   * @param string $template
   *   Template to get parts for. i.e. "node--page".
   *
   * @return mixed
   *   Return array of template parts.
   */
  private function getTemplateParts(string $template): array {
    // Explode into array of elements, using "--" as a delimiter.
    // Example: "node--page" becomes ['node', 'page'].
    $parts = explode('--', $template);

    $result = [];

    // Don't ask. Just read the function's documentation!
    foreach ($parts as $i => $part) {
      $needle = $part;

      if ($i !== 0) {
        $needle = '';
        for ($c = 0; $c <= $i; $c++) {
          $needle .= $parts[$c];
          if ($c !== $i) {
            $needle .= '--';
          }
        }
      }
      $result[] = $needle;
    }

    // Return the formatted result.
    return $result;
  }

}
