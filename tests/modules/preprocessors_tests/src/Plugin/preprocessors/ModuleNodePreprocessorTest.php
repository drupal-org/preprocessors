<?php

namespace Drupal\preprocessors_tests_theme\Plugin\preprocessors;

use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorPluginBase;

/**
 * Test implementation of a Preprocessor Plugin.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Preprocessor(
 *   id = "preprocessors_tests.module_node_preprocessor_test",
 *   template = "node"
 * )
 */
class ModuleNodePreprocessorTest extends PreprocessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info) {
    $variables['module_preprocess_test'] = 'Hello world! This is provided by the Module Node Preprocessor Test Plugin!';
  }

}
