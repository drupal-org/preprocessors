<?php

namespace Drupal\preprocessors_tests_theme\Plugin\Preprocess;

use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorPluginBase;

/**
 * Test implementation of a Preprocessor Plugin.
 */
class ThemeNodePreprocessorTest extends PreprocessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info) {
    $variables['theme_preprocess_test'] = 'Hello world! This is provided by the Theme Node Preprocessor Test Plugin!';
  }

}
