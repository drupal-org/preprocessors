<?php

namespace Drupal\Tests\preprocess\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Theme\MissingThemeDependencyException;
use Drupal\user\Entity\User;
use function array_key_exists;

/**
 * Tests main module functionality.
 *
 * @group Preprocessors
 */
class PreprocessorsTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'node',
    'preprocessors',
    'preprocessors_tests',
  ];

  /**
   * The module's plugin manager.
   *
   * @var \Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorsPluginManager
   */
  private $preprocessorsPluginManager;

  /**
   * The node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * The node view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $nodeViewBuilder;

  /**
   * The theme manager.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Theme initialization service.
   *
   * @var \Drupal\Core\Theme\ThemeInitializationInterface
   */
  protected $themeInitializer;

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  protected function setUp() {
    parent::setUp();
    $this->preprocessorsPluginManager = $this->container->get('plugin.manager.preprocessors');
    $this->themeManager = $this->container->get('theme.manager');
    $this->themeInitializer = $this->container->get('theme.initialization');
    $this->nodeViewBuilder = $this->container->get('entity_type.manager')->getViewBuilder('node');
    $this->container->get('theme_installer')->install(['preprocessors_tests_theme']);

    $type = NodeType::create([
      'type' => 'page',
      'name' => 'Basic Page',
    ]);
    $type->save();

    $this->installSchema('node', 'node_access');
    $this->installConfig(['system', 'node']);
    $this->config('system.theme')->set('default', 'preprocessors_tests_theme')->save();
  }

  /**
   * Test the discovery of preprocessors in themes and modules.
   *
   * @param string $plugin_id
   *   The id of the plugin that should be discovered.
   * @param string $theme_name
   *   The name of the theme to use as the active theme.
   * @param bool $expected
   *   Whether or not the plugin is expected to be discovered.
   *
   * @dataProvider discoveryData
   */
  public function testDiscovery(string $plugin_id, string $theme_name, bool $expected): void {
    try {
      $activeTheme = $this->themeInitializer->getActiveThemeByName($theme_name);
    }
    catch (MissingThemeDependencyException $exception) {
      $this->fail($exception->getMessage());
      return;
    }

    $this->themeManager->setActiveTheme($activeTheme);
    self::assertSame($expected, array_key_exists($plugin_id, $this->preprocessorsPluginManager->getDefinitions()));
  }

  /**
   * Test node preprocessing.
   */
  public function testNodePreprocess(): void {
    $account = User::create([
      'name' => $this->randomString(),
    ]);
    $account->save();

    $node = Node::create([
      'type' => 'page',
      'title' => $this->randomMachineName(),
      'uid' => $account->id(),
    ]);
    $node->save();

    $build = $this->nodeViewBuilder->view($node);
    $this->render($build);
    $this->assertRaw('Hello world.', $this->getRawContent());
  }

  /**
   * Data provider for testDiscovery().
   */
  public function discoveryData(): array {
    return [
      'Module plugin MUST be discovered in tests theme.' => [
        'preprocessors_tests.module_node_preprocessor_test',
        'preprocessors_tests_theme',
        TRUE,
      ],
      'Theme plugin MUST be discovered in tests theme.' => [
        'preprocessors_tests_theme.theme_node_preprocessor_test',
        'preprocessors_tests_theme',
        TRUE,
      ],
      'Theme plugin MUST NOT be discovered in core theme.' => [
        'preprocessors_tests_theme.theme_node_preprocessor_test',
        'core',
        FALSE,
      ],
      'Module plugin MUST be discovered in core theme.' => [
        'preprocessors_tests.module_node_preprocessor_test',
        'core',
        TRUE,
      ],
    ];
  }

}
