<?php

namespace Drupal\preprocessors_examples\Plugin\preprocessors;

use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorPluginBase;

/**
 * Provide plugin to alter variables for "page" type nodes.
 *
 * @Preprocessor(
 *   id = "preprocessors_example_page_preprocessor",
 *   template = "page"
 * )
 *
 * @package Drupal\preprocessors_example\Plugin\Preprocessor
 */
final class PagePreprocess extends Preprocessor {

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info): void {
    $variables['hello'] = 'world';
  }

}
