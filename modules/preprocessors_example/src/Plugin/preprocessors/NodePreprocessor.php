<?php

namespace Drupal\preprocessors_example\Plugin\preprocessors;

use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorPluginBase;

/**
 * Provide plugin to alter variables for "page" type nodes.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Preprocessor(
 *   id = "preprocessors_example.node_preprocessor",
 *   template = "node"
 * )
 *
 * @package Drupal\preprocessors_example\Plugin\Preprocessor
 */
final class NodePreprocessor extends PreprocessorPluginBase {

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info): void {
    $variables['hello'] = 'world';
  }

}
