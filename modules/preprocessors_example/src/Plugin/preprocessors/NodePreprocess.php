<?php

namespace Drupal\preprocessors_example\Plugin\preprocessors;

use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\Preprocessor;

/**
 * Provide plugin to alter variables for "page" type nodes.
 *
 * @Preprocessor(
 *   id = "preprocessors_example_node_preprocessor",
 *   template = "node"
 * )
 *
 * @package Drupal\preprocessors_example\Plugin\Preprocessor
 */
final class NodePreprocess extends Preprocessor {

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info): void {
    $variables['hello'] = 'world';
  }

}
