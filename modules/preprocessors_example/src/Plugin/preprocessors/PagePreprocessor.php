<?php

namespace Drupal\preprocessors_examples\Plugin\preprocessors;

use Drupal\preprocessors\PluginManager\PreprocessorsPluginManager\PreprocessorPluginBase;

/**
 * Provide plugin to alter variables for "page" type nodes.
 *
 * @noinspection
 *   AnnotationMissingUseInspection
 *
 * @Preprocessor(
 *   id = "preprocessors_example.page_preprocessor",
 *   template = "page"
 * )
 *
 * @package Drupal\preprocessors_example\Plugin\Preprocessor
 */
final class PagePreprocessor extends PreprocessorPluginBase {

  /**
   * Add personal tweaks to variables in this function.
   *
   * {@inheritdoc}
   */
  public function preprocess(&$variables, $hook, $info): void {
    $variables['hello'] = 'world';
  }

}
